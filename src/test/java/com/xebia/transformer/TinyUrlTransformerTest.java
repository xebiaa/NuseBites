package com.xebia.transformer;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertThat;

import java.util.HashMap;

import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.Message;
import org.springframework.integration.MessageHeaders;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.QueueChannel;

import spring.test.TemporarySpringContext;

public class TinyUrlTransformerTest {

	@Autowired
	DirectChannel transformerInputChannel;

	@Autowired
	QueueChannel transformerOutputChannel;

	@Rule
	public TemporarySpringContext context = new TemporarySpringContext(
			"/nuse-url-interceptor-test.xml");

	@Test
	public void testTransformation() throws Exception {
		transformerInputChannel.send(new Message<String>() {

			@Override
			public MessageHeaders getHeaders() {
				return new MessageHeaders(new HashMap<String, Object>());
			}

			@Override
			public String getPayload() {
				return "original www.xebia.com message http://www.xebia.com";
			}
		});

		Message<?> transformedMessage = transformerOutputChannel.receive();
		assertNotNull(transformedMessage);

		assertNotSame((String) transformedMessage.getPayload(),
				("original www.xebia.com message http://www.xebia.com"));
		assertThat((String) transformedMessage.getPayload(),
				containsString("tinyurl.com"));
	}
}
