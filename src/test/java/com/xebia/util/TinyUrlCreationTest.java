package com.xebia.util;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import org.apache.commons.httpclient.HttpException;
import org.junit.Test;

public class TinyUrlCreationTest {

	@Test
	public void testTinyUrlCreationTest() throws Exception {
		String actualUrl = "http://www.mularien.com/blog/2008/12/04/tutorial-accessing-the-tinyurl-api-from-java/";
		String tinyUrl = TinyUrlCreator.getTinyUrl(actualUrl);

		assertNotNull(tinyUrl);
		assertThat(actualUrl.length() > tinyUrl.length(), is(true));
		System.out.println(tinyUrl);
	}
	
	@Test
	public void testForInvalidUrl() throws Exception {
		String tinyUrl = TinyUrlCreator.getTinyUrl("abcdef");
		System.out.println(tinyUrl);
	}

}
