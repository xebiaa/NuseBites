package com.xebia.message.splitter;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

public class FileMessageSplitterTest {

	@Test
	public void shouldSplitTheFileMessageCorrectly() throws IOException{
		FileMessageSplitter fileMessageSplitter = new FileMessageSplitter();
		File file = new File("test1.txt");
		FileUtils.writeLines(file, Arrays.asList("test","test1","test2"));
		assertEquals(3 , fileMessageSplitter.splitMessage(file).size());
	}
	
}
