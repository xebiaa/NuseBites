package com.xebia;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.HashMap;

import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.Message;
import org.springframework.integration.MessageHeaders;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.core.PollableChannel;

import spring.test.TemporarySpringContext;

public class FilterTest {

	@Rule
	public TemporarySpringContext context = new TemporarySpringContext(
			"/nuse-twitter-filter-test.xml");

	@Autowired
	DirectChannel filterInputChannel;

	@Autowired
	QueueChannel filterAcceptenceChannel;

	@Autowired
	PollableChannel filterRejectionChannel;

	@Test
	public void testFilter() throws Exception {
		for (int i = 0; i < 10; i++) {
			filterInputChannel.send(new Message<String>() {

				@Override
				public MessageHeaders getHeaders() {
					return new MessageHeaders(new HashMap<String, Object>());
				}

				@Override
				public String getPayload() {
					return "twitter example message 1  twitter example message2 twitter example message3 twitter example message4 twitter example message5 twitter example message6 twitter example message7 twitter example message8 twitter example message9 twitter example message10 twitter example message11 twitter example message12 twitter example message13 twitter example message14";
				}
			});
		}
		for (int i = 0; i < 6; i++) {
			assertNotNull(filterAcceptenceChannel.receive(100));
		}

		for (int i = 0; i < 4; i++) {
			assertNull(filterAcceptenceChannel.receive(100));
			assertNotNull(filterRejectionChannel.receive(100));
		}
	}
}
