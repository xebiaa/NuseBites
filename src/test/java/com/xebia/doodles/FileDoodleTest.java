package com.xebia.doodles;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.core.PollableChannel;
import spring.test.TemporarySpringContext;

import java.io.File;
import java.util.Collections;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * @author iwein
 */
@Ignore
public class FileDoodleTest {

    private static TemporaryFolder temp = new TemporaryFolder();
    private static File inputDirectory;

    @BeforeClass public static void createTempFolder () throws Exception {
        temp.create();
        inputDirectory = temp.newFolder("inputDirectory");
    }

    @AfterClass public static void cleanUpTempFolder() throws Exception {
        temp.delete();
    }

    public static Map getConf(){
        return Collections.singletonMap("directory", inputDirectory);
    }

    @Rule
    public TemporarySpringContext context = new TemporarySpringContext("/doodles/TEST-file-doodle.xml");

    @Autowired
    PollableChannel filesFound;

    @Test
    public void shouldPickUpFile() throws Exception {
        //create the file to be picked up
        new File(inputDirectory, "foo").createNewFile();
        assertThat(filesFound.receive(), is(notNullValue()));
    }
}
