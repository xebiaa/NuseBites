package com.xebia.io;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Arrays;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.Message;
import org.springframework.integration.core.PollableChannel;
import org.springframework.integration.file.FileReadingMessageSource;

import spring.test.TemporarySpringContext;

public class FileReaderIntegrationTest {
	@Rule
	public static TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Rule
	public TemporarySpringContext context = new TemporarySpringContext(
			"/TEST-FilePoller-context.xml");

	@Autowired
	FileReadingMessageSource messageSource;

	@Autowired
	@Qualifier("bites")
	PollableChannel poolChannel;

	private static File folder;

	@BeforeClass
	public static void createTestFolder() throws Exception {
		temporaryFolder.create();
		folder = temporaryFolder.newFolder("data");
	}

	@AfterClass
	public static void cleanupTestFolder() throws Exception {
		temporaryFolder.delete();
	}

	public static Properties provideConf() {
		Properties properties = new Properties();
		properties.setProperty("input.directory", folder.getAbsolutePath());
		return properties;
	}

	@Test
	public void shouldLoadTheContextWithFilePollerBean() throws Exception {
		Message<?> receive;
		addFile("Test1.txt", "Test1");
		receive = poolChannel.receive();
		assertTrue(receive.getPayload().toString().contains("Test1"));

		addFile("Test2.txt", "Test2");
		receive = poolChannel.receive();
		assertTrue(receive.getPayload().toString().contains("Test2"));

		addFile("Test3.txt", "Test3");
		receive = poolChannel.receive();
		assertTrue(receive.getPayload().toString().contains("Test3"));
	}

	@Test
	public void shouldReceiveMultipleMessages() throws Exception {
		Message<?> receive;
		addFile("Test1.txt", "Test1", "Test2", "Test3");
		
		receive = poolChannel.receive();
		assertTrue(receive.getPayload().toString().contains("Test1"));
		
		receive = poolChannel.receive();
		assertTrue(receive.getPayload().toString().contains("Test2"));
		
		receive = poolChannel.receive();
		assertTrue(receive.getPayload().toString().contains("Test3"));
	}

	private static void addFile(String string, String... data) throws Exception {
		File writingFile = temporaryFolder.newFile(string);
		FileUtils.writeLines(writingFile, Arrays.asList(data));
		// make sure you don't read a file before you're done writing to it
		writingFile.renameTo(new File(folder, string));
	}
}
