package com.xebia;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.springframework.integration.test.matcher.PayloadMatcher.hasPayload;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicReference;

import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.Message;
import org.springframework.integration.MessageHeaders;
import org.springframework.integration.MessagingException;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.core.MessageHandler;

import spring.test.TemporarySpringContext;

/**
 * @author iwein
 */
public class FullApplicationTest {
	@Rule
	public TemporarySpringContext context = new TemporarySpringContext(
			"/TEST-nuse-application.xml");

	@Autowired
	PublishSubscribeChannel bites;

	@Autowired
	DirectChannel twitterChannel;
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat(
			"EEE, d MMM yyyy HH:mm:ss Z");

	@Test
	public void shouldLoadContext() throws Exception {
		// just checking if the context loads correctly
	}

	public static interface BitesAdapter {
		void takeBite(String bite);
	}

	@Autowired
	BitesAdapter bitesAdapter;

	private int messagesReceived = 0;

	@Test
	public void shouldAcceptBites() throws Exception {
		String message = "Test Message :"
				+ dateFormat.format(Calendar.getInstance().getTime());

		// String message = "http://google.com";

		final AtomicReference<Message<?>> received = new AtomicReference<Message<?>>();
		bites.subscribe(new MessageHandler() {
			public void handleMessage(Message<?> message)
					throws MessagingException {
				received.set(message);
			}
		});
		bitesAdapter.takeBite(message);
		assertThat(((String) received.get().getPayload()), is(message));
		assertThat(received.get(), hasPayload(message));
	}

	protected void updateMessagesReceived() {
		messagesReceived++;

	}

	@Test
	public void testTweetLongUrl() throws Exception {

		bites.send(new Message<String>() {
			@Override
			public MessageHeaders getHeaders() {
				return new MessageHeaders(new HashMap<String, Object>());
			}

			@Override
			public String getPayload() {
				return "Url Message :"
						+ dateFormat.format(Calendar.getInstance().getTime())
						+ " http://www.mularien.com/blog/2008/12/04/tutorial-accessing-the-tinyurl-api-from-java/"
						+ " url ";
			}
		});
	}
}
