package com.xebia.message.splitter;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class FileMessageSplitter {

	@SuppressWarnings("unchecked")
	public List<String> splitMessage(File file) throws IOException {
		return (List<String>)FileUtils.readLines(file);
	}
}
