package com.xebia.temporary;

/**
 * @author iwein
 */
public class StubBiteSpewer {

    public String spew(){
        return "This message is just for testing, remove StubBiteSpewer before going to PROD";
    }
}
