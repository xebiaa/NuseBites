package com.xebia.main;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ApplicationRunner {

	public static void main(String[] args) throws IOException {
		FileUtils.writeStringToFile(new File(
				"target/classes/nuse-filepoller.properties"),
				"input.directory=" + args[0]);
		new ClassPathXmlApplicationContext("/nuse-application.xml");

	}

}
