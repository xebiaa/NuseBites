package com.xebia.transformer;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.commons.httpclient.HttpException;
import org.apache.commons.validator.UrlValidator;

import com.xebia.util.TinyUrlCreator;

/**
 * I'm a tiny url transformer. I'll convert the url in the message to tiny url.
 * 
 */
public class TinyUrlTransformer {

	public String transform(String message) {
		StringBuilder tranformedMessage = new StringBuilder("");
		StringTokenizer tokenizer = new StringTokenizer(message, " ");

		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			if (validUrl(token)) {
				try {
					token = TinyUrlCreator.getTinyUrl(token);
				} catch (HttpException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			tranformedMessage.append(" ").append(token);

		}

		return tranformedMessage.toString();
	}

	private boolean validUrl(String token) {
		UrlValidator urlValidator = new UrlValidator();
		return urlValidator.isValid(token);
	}

}
